import initState from '../api/products';

function products(state = initState, action) {
    switch (action.type) {
        case 'ADD_PRODUCT': {
            return [...state, action.payload];
        }

        case 'EDIT_PRODUCT': {
            return state.map(item => {
                if (item.id === action.payload.id) {
                    return {...item, ...action.payload}
                }

                return item
            });
        }

        case 'EDIT_PRODUCT_TITLE': {
            return state.map(item => {
                if (item.id === action.payload.id) {
                    item.title = action.payload.title
                }

                return item
            });
        }

        case 'DELETE_PRODUCT': {
            return state.filter(item => item.id !== action.payload);
        }

        case 'SHOW_PRODUCT': {
            return state.map(item => {
                item.active = item.id === action.payload;

                return item;
            });
        }

        case 'SAVE_PRODUCT': {
            // axios.post('url', {})

            return state;
        }

        default: {
            return state;
        }
    }
}

export default products;
