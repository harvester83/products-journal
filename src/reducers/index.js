import { combineReducers } from 'redux';

const reducers = ['categories', 'products', 'visibilityFilter'];

export default combineReducers(
    reducers.reduce((initial, name) => {
        initial[name] = require(`./${name}`).default;
        return initial;
    }, {}),
);
