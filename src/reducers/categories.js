import initState from '../api/categories.json';

function categories(state = initState, action) {
    if (action.type === 'ADD_CATEGORY') {
        return  [
            ...state,
            action.payload
        ];
    }

    // switch(action.type) {
    //     case 'SLID_OPEN': {
    //         state = action.payload;
    //         return state
    //     }
    //
    //     case 'SLID_CLOSE': {
    //         state = action.payload;
    //         return state
    //     }
    //
    //     default: {
    //         return state;
    //     }
    // }

    return state;
}

export default categories;
