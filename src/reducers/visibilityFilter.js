let initState = {
    showAddProduct: false,
    showEditProduct: true,
    showAddCategory: false,
    showEditProductTitle: null
};

function visibilityFilter (state = initState, action) {
    switch (action.type) {
        case 'SHOW_ADD_PRODUCT': {
            return {...state, showAddProduct: action.payload};
        }

        case 'SHOW_EDIT_PRODUCT': {
            return {...state, showEditProduct: action.payload};
        }

        case'SHOW_EDIT_PRODUCT_TITLE': {
            if (action.payload === state.showEditProductTitle) {
                return {...state, showEditProductTitle: null}
            }
            return {...state, showEditProductTitle: action.payload}
        }

        case 'SHOW_ADD_CATEGORY': {
            return {...state, showAddCategory: true};
        }

        case 'CLOSE_ADD_CATEGORY': {
            return {...state, showAddCategory: false};
        }

        default: {
            return state;
        }
    }
}

export default visibilityFilter;
