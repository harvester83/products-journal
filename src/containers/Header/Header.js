import React from 'react';
import Logo from './Logo/Logo';

import styles from './Header.module.css';


import avatarImg from './avatar.jpg'
import envelope from './envelope.svg'
import bell from './notification-bell.svg'

class Header extends React.Component {
    render() {
        return (
            <div className={styles._Wrap}>
                <header className={styles._Top}>
                    <Logo />

                    <div className={styles.Header__line}>
                        <div>
                            <p className={styles.Date}>Bugün, 22 Yanvar 201</p>
                            <span className={styles.Date__span}>Çərşənbə</span>
                        </div>

                        <div className={styles.Info}>
                            <div className={styles.Info__notify}>
                                <div className={styles.Icon}>
                                    <img className={styles.Icon__img} src={envelope} alt=""/>
                                </div>

                                <div className={styles.Icon}>
                                    <img className={styles.Icon__img} src={bell} alt=""/>
                                </div>
                            </div>

                            <div className={styles.Selection}>
                                Pasha Insurance
                            </div>


                            <img className={styles.Avatar__img} src={avatarImg} alt=""/>
                        </div>
                    </div>
                </header>
            </div>
        )
    }
}

export default Header;