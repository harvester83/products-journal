import React from 'react';
import {connect} from 'react-redux';
import Category from '../Category/Category.js';
import Products from '../Products/Products.js';
import AddProduct from '../Products/AddProduct.js';
import EditProduct from '../Products/EditProduct.js';
import styles from './Content.module.css';
import plus from "../../assets/plus.svg";

class Content extends React.Component {
    showAddProduct = (boolean) => {
        this.props.dispatchShowAddProduct(boolean);
    };

    render() {

        const Categories = this.props.categories.map(function(item) {
            return <Category key={item.id} classThumbItem='ThumbItem' name={item.name} />
        });

        const ProductsList = this.props.products.map(function(item) {
            return <Products key={item.id} id={item.id} name={item.title} />
        });


        return (
            <div className={styles.ContainerMain}>
                <span className={styles.Date}>08 - 18 Sentyabr 2018</span>

                <div className={styles.ThumbsWrapper}>
                    <div className={styles._Coloumn}>
                        <button className={styles.ThumbButton}> Kategoriyalar
                            <img className={styles.ThumbButton__img} src={plus} alt="#"/>
                        </button>
                        { Categories }
                    </div>

                    <div className={styles._Coloumn}>

                        <button className={styles.ThumbButton} onClick={() => this.showAddProduct(true)} > Məhsullar
                            <img className={styles.ThumbButton__img} src={plus} alt="#"/>
                        </button>

                        <AddProduct showAddProduct={this.props.showAddProduct}/>

                        <ul className='ProductList'>
                            { ProductsList }
                        </ul>
                    </div>

                    <EditProduct />
                </div>
            </div>
        )
    }
}

export default connect(
    state => ({
        categories: state.categories,
        products: state.products,
        showAddProduct: state.visibilityFilter.showAddProduct,
        showEditProduct: state.visibilityFilter.showEditProduct
    }),

    dispatch => ({
        dispatchShowAddProduct: (boolean) => {
            dispatch({type: 'SHOW_ADD_PRODUCT', payload: boolean});
        },
    })
)(Content);
