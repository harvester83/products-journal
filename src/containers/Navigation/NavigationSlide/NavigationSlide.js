import React from 'react';
import {connect} from 'react-redux'

import styles from './NavigationSlide.module.css';
import CloseSvg from '../Svg/CloseSvg.js';

class NavigationSlide extends React.Component {

    hideSlid = () => {
        this.props.onHideSlid()
    };

    addCategory = () => {
        this.props.onAddCategory(this.trackInput.value)
    };

    render() {
        return (
            <div className={`${styles.NavigationSlide} ${this.props.showAddCategory && styles.NavigationSlide_open}`}>
                <div className={`${styles.NavigationPane__box} ${styles.NavigationPane__box_flex} ${styles.NavigationPane__box_nbord}`}>
                    <h2 className={styles.NavigationPane__title}>Yeni kateqoriya</h2>

                    <button className={styles.NavigationSlide__button} onClick={this.hideSlid}>
                        <CloseSvg />
                    </button>
                </div>

                <div className={`${styles.NavigationSlide__box} ${styles.NavigationSlide__box_mb}`}>
                    <label className={`${styles.NavigationPane__label} ${styles.NavigationPane__label_min}`}>Qrup adı</label>
                    <div className={styles.SearchBox}>
                        <input className={styles.SearchBox__input} type="text" ref={(input) => {this.trackInput = input}} placeholder='Category name'/>
                    </div>
                </div>

                <div className={`${styles.NavigationSlide__box} ${styles.NavigationSlide__box_mb}`}>
                    <button className={styles.NavigationPane__button}
                            onClick={this.addCategory}
                    >
                        Əlavə et
                    </button>
                </div>
            </div>
        )
    }
}

export default connect(
    state => ({
        categories: state.categories,
        showAddCategory: state.visibilityFilter.showAddCategory
    }),

    dispatch => ({
        onHideSlid: () => {
            dispatch({type: 'CLOSE_ADD_CATEGORY'})
        },

        onAddCategory: (name) => {
            const payload = {
                id: Date.now().toString(),
                name
            };
            dispatch({type: 'ADD_CATEOGRY', payload: payload})
        }
    })
)(NavigationSlide);