import React from 'react';
import {connect} from 'react-redux';

import Icon from "./Icon/Icon";
import styles from './Navigation.module.css';
import NavigationSlide from './NavigationSlide/NavigationSlide.js';

import list from './img/list.svg';
import calc from './img/calc.svg';
import calendar from './img/calendar.svg';
import money from './img/money.svg';
import sheme from './img/sheme.svg';
import phone from './img/phone.svg';
import settings from './img/settings.svg';
import search from './img/search.svg';
import select_arrow from './img/select_arrow.svg';

class Navigation extends React.Component {

    toggleSlid = () => {
        this.props.onShowSlid(true)
    };

    render() {
        return (
            <div className={styles.Navigation}>
                <div className={styles.Navigation__wrap}>
                    <Icon text='Davamiyyət' url={list}/>
                    <Icon text='Əməkhaqqı jurnalı' url={money}/>
                    <Icon text='Dövriyyə jurnalı' url={calc}/>
                    <Icon text='Borclar jurnalı' url={calendar}/>
                    <Icon text='Hesabatlar' url={sheme}/>
                    <Icon text='Zənglər jurnalı' url={phone}/>
                </div>

                <div className={styles.NavigationPane}>
                    <div className={`${styles.NavigationPane__box} ${styles.NavigationPane__box_flex}`}>
                        <h2 className={styles.NavigationPane__title}>Məhsullar jurnalı</h2>
                        <img src={settings} alt="#"/>
                    </div>

                    <div className={styles.NavigationPane__box}>
                        <button className={styles.NavigationPane__button}
                                onClick={this.toggleSlid}
                        >
                            Yeni kateqoriya
                        </button>
                    </div>

                    <div className={styles.NavigationPane__box}>
                        <label className={styles.NavigationPane__label}>Axtarış</label>
                        <div className={styles.SearchBox}>
                            <input className={styles.SearchBox__input} type="text"/>

                            <button className={styles.SearchBox__submit}>
                                <img src={search} alt="#"/>
                            </button>
                        </div>
                    </div>

                    <div className={styles.NavigationPaneList}>
                        <div className={styles.NavigationPaneList__item}>
                            <span className={styles.NavigationPaneList__itemSpan}>Kateqoriya üzrə</span>
                            <img src={select_arrow} alt="#"/>
                        </div>
                        <ul className='d-none'>
                            <li>Kateqoriya üzrə 01</li>
                            <li>Kateqoriya üzrə 02</li>
                            <li>Kateqoriya üzrə 03</li>
                            <li>Kateqoriya üzrə 04</li>
                        </ul>
                    </div>

                    <div className={styles.NavigationPaneList}>
                        <div className={styles.NavigationPaneList__item}>
                            <span className={styles.NavigationPaneList__itemSpan}>Model seçimi</span>
                            <img src={select_arrow} alt="#"/>
                        </div>
                        <ul className='d-none'>
                            <li>Kateqoriya üzrə</li>
                        </ul>
                    </div>

                    <div className={styles.NavigationPaneList}>
                        <div className={styles.NavigationPaneList__item}>
                            <span className={styles.NavigationPaneList__itemSpan}>Məhsul tipi</span>
                            <img src={select_arrow} alt="#"/>
                        </div>
                        <ul className='d-none'>
                            <li>Kateqoriya üzrə</li>
                        </ul>
                    </div>

                    <div className={`${styles.NavigationPane__box} ${styles.NavigationPane__box_mod}`}>
                        <label className={`${styles.NavigationPane__label} ${styles.NavigationPane__label_m}`}>
                            Seçim üzrə filtrasiya
                        </label>

                        <button className={`${styles.NavigationPaneFilter__button} ${styles.active}`}>Ümumi</button>
                        <button className={styles.NavigationPaneFilter__button}>Mal / Material</button>
                        <button className={styles.NavigationPaneFilter__button}>Xidmət</button>
                    </div>
                </div>

                <NavigationSlide />
            </div>
        )
    }
}


export default connect(
    state => ({
        categories: state.categories,
    }),

    dispatch => ({
        onShowSlid: () => {
            dispatch({type: 'SHOW_ADD_CATEGORY'})
        },
    })
)(Navigation);


