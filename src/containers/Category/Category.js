import React from 'react';

import styles from './Category.module.css';
import select_arrow from '../Navigation/img/select_arrow.svg';

class Category extends React.Component {
    render() {
        return (
            <div className={`${styles[this.props.classThumbItem]} ${styles[this.props.classThumbItem_mid_w]}`}>
                <img className={styles.ThumbItem__img} src={select_arrow} alt="#"/>
                {this.props.name}
            </div>
        )
    }
}

export default Category;
