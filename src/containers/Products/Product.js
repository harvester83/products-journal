import React from 'react';
import styles from './Product.module.css';
import {connect} from "react-redux";

class Product extends React.Component {
    editProduct = (event, key) => {
        let product = this.props.activeProduct;
        product[key] = event.target.value;

        this.props.dispatchEditProduct(product);
    };

    render() {
        let productOptions = {};
        let product = this.props.activeProduct;

        for (let key in product) {
            if (key === 'id' || key === 'title' || key === 'active') {
                continue;
            }

            productOptions[key] = product[key];
        }

        return (
            Object.keys(productOptions).map((item, index) => {
                return (
                    <div className={styles.ThumbField__item} key={index}>
                        <span className={styles.ThumbField__itemOption}>{item}</span>

                        <input disabled={this.props.showEditProduct}
                               name=""
                               className={styles._ColoumnEditPane__Input}
                               type="text"
                               value={this.props.activeProduct[item]}
                               onChange={(event) => this.editProduct(event, item, index)}
                        />
                    </div>
                )
            })
        )
    }
}

export default connect(null,
    dispatch => ({
        dispatchEditProduct: (product) => {
            dispatch({type: 'EDIT_PRODUCT', payload: product})
        },
    })
)(Product);
