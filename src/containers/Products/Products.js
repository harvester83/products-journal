import React from 'react';
import styles from './Products.module.css';
import ProductsTitle from './ProductsTitle.js';
import {connect} from "react-redux";
import Edit from "../../components/Options/Edit";
import Delete from "../../components/Options/Delete";


class Products extends React.Component {

    showEditPrdouctTitle = (id) => {
        console.log(id)
        this.props.dispatchShowEditPrdouctTitle(id);
    }

    deleteProduct = (id) => {
        this.props.dispatchDeleteProduct(id);
    }

    render() {
        return (
            <li className={styles.Product}>

                <ProductsTitle id={this.props.id} name={this.props.name} toggle={this.props.showEditProductTitle}/>

                 <div className={styles.ProductButtons}>
                    <button className={styles.Product__Button} onClick={() => this.showEditPrdouctTitle(this.props.id)}>
                        <Edit className={styles.Icon} svgClassName={styles.Icon__svg}/>
                    </button>

                    <button className={styles.Product__Button} onClick={() => this.deleteProduct(this.props.id)}>
                        <Delete className={styles.Icon} svgClassName={styles.Icon__svg}/>
                    </button>
                </div>
            </li>
        )
    }
}

export default connect(
    state => ({
        products: state.products,
        showEditProductTitle: state.visibilityFilter.showEditProductTitle
    }),

    dispatch => ({
        dispatchShowEditPrdouctTitle: (id) => {
            dispatch({type: 'SHOW_EDIT_PRODUCT_TITLE', payload: id})
        },

        dispatchDeleteProduct: (id) => {
            dispatch({type: 'DELETE_PRODUCT', payload: id})
        }
    })
)(Products);
