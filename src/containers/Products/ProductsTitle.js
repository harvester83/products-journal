import React from 'react';
import styles from './Products.module.css';
import {connect} from "react-redux";


class ProductsTitle extends React.Component {

    selectProduct = (index) => {
        this.props.dispatchSelectProduct(index);
    }

    editProductTitle = (event, id) => {
        let productOpt = {
            id: id,
            title: event.target.value
        }

        this.props.dispatchEditProductTitle(productOpt)
    }

    render() {
        return (
            <div>
                <span className={`${styles.Product__Name} ${ this.props.toggle === this.props.id && styles.hide}`}
                    onClick={()=> this.selectProduct(this.props.id)}>
                    {this.props.name}
                </span>

                <input className={`${styles.Product__Input} ${ this.props.toggle === this.props.id && styles.block}`}
                       type="text"
                       value={this.props.name}
                       onChange={(event) => this.editProductTitle(event, +this.props.id)}
                />
            </div>
        )
    }
}

export default connect(null,
    dispatch => ({
        dispatchSelectProduct: (index) => {
            dispatch({type: 'SHOW_PRODUCT', payload: index})
        },

        dispatchEditProductTitle: (productOpt) => {
            dispatch({type: 'EDIT_PRODUCT_TITLE', payload: productOpt})
        }
    })
)(ProductsTitle);
