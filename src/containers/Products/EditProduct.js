import React from 'react';
import {connect} from 'react-redux';
import Product from '../Products/Product.js';
import Edit from '../../components/Options/Edit.js';
import styles from '../Content/Content.module.css';


class EditProduct extends React.Component {

    showEditProduct = (boolean) => {
        this.props.dispatchShowEditProduct(boolean);
    };

    closeEditProduct = (boolean) => {
        this.props.dispatchCloseEditProduct(boolean);
    };

    saveProduct = () => {
        this.props.dispatchSaveProduct();
    };

    render() {
        const activeProduct = this.props.products.filter(item => item.active)[0] || [];

        return (
            <div className={styles._Coloumn}>
                <div className={styles._ColoumnField}>
                    <h3 className={styles._ColoumnFieldTitle}
                        onClick={() => this.showEditProduct(false)}
                    >
                        <span>{activeProduct.title}</span>

                        <Edit className={styles.pencilIcon} svgClassName={styles.pencilIcon__svg}/>
                    </h3>

                    <div className={styles._ColoumnFieldContent}>

                        <Product activeProduct={activeProduct} showEditProduct={this.props.showEditProduct}/>

                        <div className={`${styles._EditorButtons} ${ !this.props.showEditProduct && styles.active}`}>
                            <button className={styles._ColoumnEditPane__Button}
                                    onClick={this.saveProduct}
                            >
                                Redaktə et
                            </button>

                            <button className={styles._ColoumnEditPane__Button_simple}
                                    onClick={() => this.closeEditProduct(true)}
                            >
                                İmtina
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default connect(
    state => ({
        categories: state.categories,
        products: state.products,
        showEditProduct: state.visibilityFilter.showEditProduct
    }),

    dispatch => ({
        dispatchShowEditProduct: (boolean) => {
            dispatch({type: 'SHOW_EDIT_PRODUCT', payload: boolean});
        },

        dispatchCloseEditProduct: (boolean) => {
            dispatch({type: 'SHOW_EDIT_PRODUCT', payload: boolean});
        },

        dispatchSaveProduct: () => {
            dispatch({type: 'SAVE_PRODUCT'});
        }
    })
)(EditProduct);
