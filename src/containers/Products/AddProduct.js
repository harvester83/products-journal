import React from 'react';
import styles from './AddProduct.module.css';
import {connect} from "react-redux";

class AddProduct extends React.Component {
    getInputValue = () => {
        if (this.trackInput.value !== '') {
            this.props.onGetInputValue(this.trackInput.value);
        }

        this.trackInput.value = ''
    };

    closeAddProduct = (boolean) => {
        this.props.dispatchCloseAddProduct(boolean);
    };

    render() {
        return (
            <div className={`${styles._Field} ${ this.props.showAddProduct && styles.active}`}>
                <input className={styles._Input}
                       type="text"
                       placeholder="Məhsulun adı"
                       ref={(input) => {this.trackInput = input}}
                />

                <div className={styles._ButtonsWrap}>
                    <button className={`${styles._Button} ${styles._Button_accept}`} onClick={this.getInputValue}>Əlavə et</button>
                    <button className={styles._Button} onClick={() => this.closeAddProduct(false)}>İmtina</button>
                </div>
            </div>
        )
    }
}

export default connect(
    state => ({
        products: state.products
    }),

    dispatch => ({
        onGetInputValue: (value) => {
            const product = {
                id: Date.now().toString(),
                title: value,
                dimensions: '',
                cpu: '',
                hdd: '',
                ram: '',
                sale_price: '',
                dealer_price: '',
                partner: '',
                img: '',
                note: ''
            };

            dispatch({type: 'ADD_PRODUCT', payload: product})
        },

        dispatchCloseAddProduct: () => {
            dispatch({type: 'SHOW_ADD_PRODUCT', payload: false})
        }
    })
)(AddProduct);
