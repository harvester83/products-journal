import React from 'react';
import './App.css';
import Header from './containers/Header/Header';
import Navigation from './containers/Navigation/Navigation';
import Content from "./containers/Content/Content";

class App extends React.Component {

    render() {
        return (
            <div className="App">
                <Header/>

                <Navigation/>

                <Content />
            </div>
        );
    }
}

export default App;
